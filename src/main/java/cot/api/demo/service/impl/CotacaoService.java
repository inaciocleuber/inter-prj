package cot.api.demo.service.impl;

import cot.api.demo.adapter.ICotacaoAdapter;
import cot.api.demo.service.ICotacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CotacaoService implements ICotacaoService {

    @Autowired
    private ICotacaoAdapter iCotacaoAdapter;


    @Override
    public Double solicitarCotacao() {
        return iCotacaoAdapter.solicitarCotacao();
    }
}
