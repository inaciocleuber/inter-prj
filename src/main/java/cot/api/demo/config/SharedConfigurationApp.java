package cot.api.demo.config;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan({
        "cot.api.demo.service",
        "cot.api.demo.service.impl",
        "cot.api.demo.model",
        "cot.api.demo.model.mapper",
        "cot.api.demo.controller"
})
@EntityScan("cot.api.demo.entity")
@EnableJpaRepositories(basePackages="cot.api.demo.repository")
public class SharedConfigurationApp {
}
