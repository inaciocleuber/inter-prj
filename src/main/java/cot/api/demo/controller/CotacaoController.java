package cot.api.demo.controller;


import cot.api.demo.service.ICotacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cotacao")
public class CotacaoController {

    @Autowired
    private ICotacaoService iCotacaoService;

    @GetMapping("")
    public ResponseEntity<Object> solicitarCotacao()
    {
        var cot = iCotacaoService.solicitarCotacao();
        return ResponseEntity.status(HttpStatus.OK).body(cot);
    }
}
