package cot.api.demo.model.mapper;

import cot.api.demo.entity.TransacaoEntity;
import cot.api.demo.model.Transacao;
import lombok.Generated;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Generated
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface TransacaoMapper {

    Transacao toTransacaoEntity(TransacaoEntity transacaoEntity);
    TransacaoEntity toTransacaoModel(Transacao transacao);

}
