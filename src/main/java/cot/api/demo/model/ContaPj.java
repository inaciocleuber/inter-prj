package cot.api.demo.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ContaPj extends ContaBase {

    private String cnpj;
}
