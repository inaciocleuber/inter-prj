package cot.api.demo.entity;

import jakarta.persistence.*;
import lombok.*;
import java.util.UUID;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TB_Carteira")
public class CarteiraEmntity {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID idCarteira;

    @Column(name = "Saldo_Br")
    private Double saldoBr;

    @Column(name = "Saldo_Usd")
    private Double saldoUsd;

    @Column(name ="Registro")
    private String registro;

}
