package cot.api.demo.entity;


import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TB_Conta_Trans")
public class TransacaoEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID idTransacao;

    @Column(name = "Descricao")
    private String descricao;

    @Column(name = "Conta_Origem")
    private String contaOrigem;

    @Column(name = "Conta_Destino")
    private String contaDestino;

    @Column(name = "Valor")
    private Double valor;

    @Column(name = "Data_Trans")
    private LocalDateTime dataTransacao;
}
