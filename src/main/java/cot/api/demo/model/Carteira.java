package cot.api.demo.model;


import lombok.*;

import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Carteira {

    private UUID idCarteira;

    private Double saldoBr;

    private Double saldoUsd;

    private String registro;
}
