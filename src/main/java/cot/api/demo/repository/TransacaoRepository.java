package cot.api.demo.repository;

import cot.api.demo.entity.TransacaoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface TransacaoRepository extends JpaRepository<TransacaoEntity, UUID> {

    @Query(value = "SELECT SUM(c.Valor) FROM TB_Conta_Trans c where c.Data_Trans = :dataTransacao", nativeQuery = true)
    Double findAllByDataTransacao(@Param(value = "dataTransacao")LocalDateTime dataTransacao);
}
