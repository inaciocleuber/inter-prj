package cot.api.demo.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TB_Conta_Pf")
@AttributeOverride(name ="id", column = @Column(name = "ID"))
public class ContaPfEntity extends ContaBaseEntity {

    @NotBlank(message = "O cpf deve ser informado")
    @Size(min = 11, max = 11, message = "O cpf deve conter 11 caracteres")
    @Column(name = "Cpf", unique = true)
    private String cpf;

}
