package cot.api.demo.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ContaPf extends ContaBase {

    private String cpf;
}
