package cot.api.demo;

import cot.api.demo.entity.CarteiraEmntity;
import cot.api.demo.entity.ContaPfEntity;
import cot.api.demo.entity.ContaPjEntity;
import cot.api.demo.model.Carteira;
import cot.api.demo.model.ContaPf;
import cot.api.demo.model.ContaPj;
import cot.api.demo.model.mapper.CarteiraMapper;
import cot.api.demo.model.mapper.ContaPjMapper;
import cot.api.demo.repository.CarteiraRepository;
import cot.api.demo.repository.ContaPjRepository;
import cot.api.demo.service.impl.ContaPjServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class ContaPjTest {
    @InjectMocks
    private ContaPjServiceImpl contaPjService;

    @Mock
    private ContaPjRepository contaPjRepository;

    @Mock
    private CarteiraRepository carteiraRepository;

    @Mock
    private ContaPjMapper contaPfMapper;

    @Mock
    private CarteiraMapper carteiraMapper;

    @Test
    void salvarContaPf(){

        dadosPf result = getDadosPf();

        when(carteiraMapper.toCarteiraModel(result.carteira())).thenReturn(result.carteiraEntity());
        when(contaPfMapper.toContaPjEntity(result.contaPj())).thenReturn(result.entity());
        when(contaPfMapper.toContaPjModel(result.entity())).thenReturn(result.contaPj());
        when(this.carteiraRepository.save(result.carteiraEntity())).thenReturn(result.carteiraEntity());
        when(this.contaPjRepository.save(result.entity())).thenReturn(result.entity());


        var conta = this.contaPjService.salvarRemessaPj(result.contaPj());

        Assertions.assertEquals(conta, result.contaPj());

    }

    private record dadosPf(Carteira carteira, CarteiraEmntity carteiraEntity, ContaPj contaPj, ContaPjEntity entity) {
    }

    @Test
    void recuperarPf(){

        dadosPf result = getDadosPf();

        when(carteiraMapper.toCarteiraModel(result.carteira)).thenReturn(result.carteiraEntity);
        when(contaPfMapper.toContaPjEntity(result.contaPj)).thenReturn(result.entity);
        when(contaPfMapper.toContaPjModel(result.entity)).thenReturn(result.contaPj);
        when(this.contaPjRepository.findFirstByCnpj("11111111111111")).thenReturn(Optional.of(result.entity));

        var retorno = this.contaPjService.recuperarConta("11111111111111");
        Assertions.assertEquals(result.contaPj, retorno);


    }

    private static dadosPf getDadosPf() {
        UUID uuid = UUID.randomUUID();

        Carteira carteira = new Carteira();
        carteira.setSaldoUsd(10.0);
        carteira.setSaldoBr(11.0);

        CarteiraEmntity carteiraEntity = new CarteiraEmntity();
        carteiraEntity.setIdCarteira(UUID.randomUUID());
        carteiraEntity.setSaldoUsd(10.0);
        carteiraEntity.setSaldoBr(11.0);

        ContaPj contaPj = new ContaPj();
        contaPj.setNome("Inacio");
        contaPj.setCnpj("11111111111111");
        contaPj.setSenha("12345");
        contaPj.setEmail("teste@cv.com");
        contaPj.setCarteira(carteira);

        ContaPjEntity entity = new ContaPjEntity();
        entity.setId(uuid);
        entity.setNome("Inacio");
        entity.setCnpj("11111111111111");
        entity.setSenha("12345");
        entity.setEmail("teste@cv.com");
        entity.setCarteira(carteiraEntity);
        dadosPf result = new dadosPf(carteira, carteiraEntity, contaPj, entity);
        return result;
    }
}
