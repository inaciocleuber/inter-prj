package cot.api.demo.model.mapper;



import cot.api.demo.entity.ContaPfEntity;
import cot.api.demo.model.ContaPf;
import lombok.Generated;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;



@Generated
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ContaPfMapper {

    ContaPfEntity toContaPfEntity(ContaPf contaPf);
    ContaPf toContaPfModel(ContaPfEntity contaPfEntity);

}
