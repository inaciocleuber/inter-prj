package cot.api.demo.model;

import jakarta.persistence.Column;
import lombok.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transacao {

    private UUID idTransacao;

    private String descricao;

    private String contaOrigem;

    private String contaDestino;

    private Double valor;

    private LocalDateTime dataTransacao;
}
