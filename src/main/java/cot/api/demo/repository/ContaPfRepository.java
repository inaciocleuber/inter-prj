package cot.api.demo.repository;

import cot.api.demo.entity.ContaPfEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContaPfRepository extends JpaRepository<ContaPfEntity, UUID> {


    Optional<ContaPfEntity> findFirstByCpf(String cpf);
}
