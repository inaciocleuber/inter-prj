package cot.api.demo.entity;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TB_Conta_Pj")
@AttributeOverride(name ="id", column = @Column(name = "ID"))
public class ContaPjEntity extends ContaBaseEntity {

    @NotBlank(message = "O cnpj deve ser informado")
    @Size(min = 14, max = 14, message = "O cnpj deve conter 14 caracteres")
    @Column(name = "Cnpj", unique = true)
    private String cnpj;
}
