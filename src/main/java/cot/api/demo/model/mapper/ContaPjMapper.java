package cot.api.demo.model.mapper;


import cot.api.demo.entity.ContaPjEntity;
import cot.api.demo.model.ContaPj;
import lombok.Generated;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Generated
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ContaPjMapper {

    ContaPjEntity toContaPjEntity(ContaPj contaPj);
    ContaPj toContaPjModel(ContaPjEntity contaPjEntity);
}
