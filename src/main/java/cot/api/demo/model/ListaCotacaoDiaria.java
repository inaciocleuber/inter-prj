package cot.api.demo.model;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaCotacaoDiaria {

    private List<CotacaoDiaria> value;
}
