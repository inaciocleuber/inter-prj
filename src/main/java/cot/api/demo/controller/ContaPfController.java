package cot.api.demo.controller;

import cot.api.demo.model.ContaPf;
import cot.api.demo.service.IContaPfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/contaPf")
public class ContaPfController {

    @Autowired
    private IContaPfService service;

    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody ContaPf contaPf)
    {
        var conta =  service.salvarRemessaPf(contaPf);
        return ResponseEntity.status(HttpStatus.OK).body(conta);

    }

    @GetMapping("/{cpf}")
    public ResponseEntity<Object> recuperarConta(@PathVariable("cpf")String cpf){
        var conta = service.recuperarConta(cpf);
        return ResponseEntity.status(HttpStatus.OK).body(conta);
    }


}
