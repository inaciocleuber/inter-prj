package cot.api.demo.service;

import cot.api.demo.model.Transacao;

public interface ITransacaoService {

    Transacao realizaTransacao(Transacao transacao);
}
