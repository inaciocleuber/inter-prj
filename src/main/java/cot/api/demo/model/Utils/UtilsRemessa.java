package cot.api.demo.model.Utils;


import java.time.DayOfWeek;
import java.time.LocalDate;

public class UtilsRemessa {

    public boolean isCPF(String cpf) {
        // Remove caracteres não numéricos
        cpf = cpf.replaceAll("[^0-9]", "");

        // Verifica se o CPF tem 11 dígitos
        if (cpf.length() != 11)
            return false;

        // Verifica se todos os dígitos são iguais, o que é inválido para um CPF
        if (cpf.matches("(\\d)\\1{10}"))
            return false;

        return true;
    }

    public boolean isCNPJ(String cnpj) {
        // Remove caracteres não numéricos
        cnpj = cnpj.replaceAll("[^0-9]", "");

        // Verifica se o CNPJ tem 14 dígitos
        if (cnpj.length() != 14)
            return false;

        // Verifica se todos os dígitos são iguais, o que é inválido para um CNPJ
        if (cnpj.matches("(\\d)\\1{13}"))
            return false;

        return true;
    }

    public double convertToUSD(double amountInBRL, Double USD_TO_BRL_RATE) {
        return amountInBRL / USD_TO_BRL_RATE;
    }

    public  boolean isDiaDeSemana() {
        LocalDate data = LocalDate.now();
        // Obtém o dia da semana da data fornecida
        DayOfWeek diaDaSemana = data.getDayOfWeek();

        // Verifica se não é sábado nem domingo
        return diaDaSemana != DayOfWeek.SATURDAY && diaDaSemana != DayOfWeek.SUNDAY;
    }


}
