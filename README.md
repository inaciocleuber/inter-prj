# Desafio técnico desenvolvedor backend

## Stack:

    - Linguagem Java 17
    - Framework: Spring
    - Tooling: Maven | Docker

## Deploy: Executando os testes

Via terminal

```shell
./mvnw test

# Caso o comando acima não execute, mude as permissões do mvnw
sudo chmod +x mvnw
```

## Deploy: Executando o Projeto

### Compilando container da api e iniciando projeto

```bash
docker compose up -d --build
```

## Acessando API

### Via browser:

- [Swagger](http://localhost:8080/swagger-ui/index.html)

### Via terminal:

```shell
curl localhost:8080/cotacao
```
## Passo a passo para utilização do seriço
- Necessário criar duas contas, que logo, será criada duas carteias (pode ser criado pessoa fisica ou pessoa juridica sem restriçoes)
- Verificar no banxo H2 em memória http://localhost:8080/h2-console, a criação dos registros
- Após criado as contas, com seu devidos saldos nas duas moedas, pode ser realizado uma transação
- Rodar a collection transação com os documentos(cpf/cnpj) destino e origem
- Pode ser executado via swagger tambem conforme informações acima

## Importando Collection postman
Deverá ser importado o seguinte arquivo: *Inter Collection.postman_collection* presente na raiz do diretorio


## Objetivo: Serviço de Remessa

- Temos 2 tipos de usuários: Pessoa Física (PF) e Pessoa Jurídica (PJ)
- Ambos possuem carteira em Real e em Dólar e realizam remessa entre eles.

### Requisitos:

- Deverá ser criado uma API RESTFul que fará uma remessa entre os
  usuários

  - Uma remessa constitui de uma operação de conversão de moeda de Real para Dólar e depois de uma operação de transferência do valor convertido.

- A cotação da moeda deverá ser consultada através da API publica

  - https://dadosabertos.bcb.gov.br/dataset/dolaramericano-usd-todos-osboletins-diarios/resource/22ab054cb3ff-4864-82f7-b2815c7a77ec
  - Obs.: utilizar o campo “cotacaoCompra” da resposta da API.

- Utilizar um banco em memória de sua preferência.

- Os usuários deverão ter uma carteira de saldo em Real e saldo em Dólar.

- Validar se o usuário tem saldo antes da remessa.

- Os usuários deverão ser criados com nome completo, e-mail, senha, CPF para PF e CNPJ para PJ.

- Os e-mails, CPF e CNPJ’s deverão ser únicos.

- PF’s possuem um limite de 10 mil reais transacionados por dia.

- PJ’s possuem um limite de 50 mil reais transacionados por dia.

- Não há restrição de remessa entre PF’s e PJ’s e vice-versa.

- A cotação de moeda não funciona aos finais de semana, sendo assim, o serviço deverá consultar a última cotação de moeda obtida.

- A operação de transferência deve ser uma transação (ou seja, revertida em qualquer caso de inconsistência) e o dinheiro deve voltar para a carteira do usuário que envia.
