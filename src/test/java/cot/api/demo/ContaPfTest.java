package cot.api.demo;

import cot.api.demo.entity.CarteiraEmntity;
import cot.api.demo.entity.ContaPfEntity;
import cot.api.demo.model.Carteira;
import cot.api.demo.model.ContaPf;
import cot.api.demo.model.mapper.CarteiraMapper;
import cot.api.demo.model.mapper.ContaPfMapper;
import cot.api.demo.repository.CarteiraRepository;
import cot.api.demo.repository.ContaPfRepository;
import cot.api.demo.service.impl.ContaPfServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
public class ContaPfTest {

    @InjectMocks
    private ContaPfServiceImpl contaPfService;

    @Mock
    private ContaPfRepository contaPfRepository;

    @Mock
    private CarteiraRepository carteiraRepository;

    @Mock
    private ContaPfMapper contaPfMapper;

    @Mock
    private CarteiraMapper carteiraMapper;

    @Test
    void salvarContaPf(){

        UUID uuid = UUID.randomUUID();

        dadosPf result = getDadosPf(uuid);

        when(carteiraMapper.toCarteiraModel(result.carteira)).thenReturn(result.carteiraEntity);
        when(contaPfMapper.toContaPfEntity(result.contaPf)).thenReturn(result.entity);
        when(contaPfMapper.toContaPfModel(result.entity)).thenReturn(result.contaPf);
        when(this.carteiraRepository.save(result.carteiraEntity)).thenReturn(result.carteiraEntity);
        when(this.contaPfRepository.save(result.entity)).thenReturn(result.entity);


        var conta = this.contaPfService.salvarRemessaPf(result.contaPf);

        Assertions.assertEquals(conta, result.contaPf);

    }

    @Test
    void recuperarPf(){

        UUID uuid = UUID.randomUUID();

        dadosPf result = getDadosPf(uuid);

        when(carteiraMapper.toCarteiraModel(result.carteira())).thenReturn(result.carteiraEntity());
        when(contaPfMapper.toContaPfEntity(result.contaPf())).thenReturn(result.entity());
        when(contaPfMapper.toContaPfModel(result.entity())).thenReturn(result.contaPf());
        when(this.contaPfRepository.findFirstByCpf("12345678910")).thenReturn(Optional.of(result.entity()));

        var retorno = this.contaPfService.recuperarConta("12345678910");
        Assertions.assertEquals(result.contaPf(), retorno);


    }

    private static dadosPf getDadosPf(UUID uuid) {
        Carteira carteira = new Carteira();
        carteira.setSaldoUsd(10.0);
        carteira.setSaldoBr(11.0);

        ContaPf contaPf = new ContaPf();
        contaPf.setNome("Inacio");
        contaPf.setCpf("12345678910");
        contaPf.setSenha("12345");
        contaPf.setEmail("teste@cv.com");
        contaPf.setCarteira(carteira);

        CarteiraEmntity carteiraEntity = new CarteiraEmntity();
        carteiraEntity.setIdCarteira(UUID.randomUUID());
        carteiraEntity.setSaldoUsd(10.0);
        carteiraEntity.setSaldoBr(11.0);

        ContaPfEntity entity = new ContaPfEntity();
        entity.setId(uuid);
        entity.setNome("Inacio");
        entity.setCpf("12345678910");
        entity.setSenha("12345");
        entity.setEmail("teste@cv.com");
        entity.setCarteira(carteiraEntity);
        dadosPf result = new dadosPf(carteira, contaPf, carteiraEntity, entity);
        return result;
    }

    private record dadosPf(Carteira carteira, ContaPf contaPf, CarteiraEmntity carteiraEntity, ContaPfEntity entity) {
    }


}
