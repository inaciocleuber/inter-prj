package cot.api.demo.repository;

import cot.api.demo.entity.ContaPjEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContaPjRepository extends JpaRepository<ContaPjEntity, UUID> {

    Optional<ContaPjEntity> findFirstByCnpj(String cnpj);
}
