package cot.api.demo.service.impl;

import cot.api.demo.model.Carteira;
import cot.api.demo.model.Utils.UtilsRemessa;
import cot.api.demo.model.mapper.CarteiraMapper;
import cot.api.demo.repository.CarteiraRepository;
import cot.api.demo.service.ICarteiraService;
import cot.api.demo.service.IContaPfService;
import cot.api.demo.service.IContaPjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CarteiraServiceImpl implements ICarteiraService {

    @Autowired
    private CarteiraRepository carteiraRepository;

    @Autowired
    private IContaPfService iContaPfService;

    @Autowired
    private IContaPjService iContaPjService;


    @Autowired
    private CarteiraMapper carteiraMapper;

    @Override
    public Carteira atualizarCarteira(UUID id, Double saldoBr, Double saldoUsd) {
        var carteiraEtity = carteiraRepository.findById(id).orElseThrow();

        carteiraEtity.setSaldoBr(saldoBr);
        carteiraEtity.setSaldoUsd(saldoUsd);

        var rec = carteiraRepository.save(carteiraEtity);
        return carteiraMapper.toCarteiraEntity(rec);

    }

    @Override
    public Double verificarSaldo(String documento) {
        UtilsRemessa utils = new UtilsRemessa();
        if(utils.isCPF(documento))
            return iContaPfService.verificaSaldoPf(documento);
        else
            return  iContaPjService.verificaSaldoPj(documento);

    }

    @Override
    public Carteira recuperaCarteira(String registro) {

        return carteiraMapper.toCarteiraEntity(carteiraRepository.findFirstByRegistro(registro).orElseThrow());
    }
}
