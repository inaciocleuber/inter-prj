package cot.api.demo.service.impl;

import cot.api.demo.model.CotacaoConfig;
import cot.api.demo.model.Transacao;
import cot.api.demo.model.Utils.UtilsRemessa;
import cot.api.demo.model.mapper.TransacaoMapper;
import cot.api.demo.repository.TransacaoRepository;
import cot.api.demo.service.ICarteiraService;
import cot.api.demo.service.ICotacaoService;
import cot.api.demo.service.ITransacaoService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TransacaoServiceImpl implements ITransacaoService {

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private ICarteiraService iCarteiraService;

    @Autowired
    private ICotacaoService iCotacaoService;

    @Autowired
    private TransacaoMapper transacaoMapper;

    @Autowired
    private CotacaoConfig cotacaoConfig;


    @Transactional
    @Override
    public Transacao realizaTransacao(Transacao transacao) {

        UtilsRemessa utilsRemessa = new UtilsRemessa();

        //verificar saldo
        validarSaldo(transacao.getValor(), transacao.getContaOrigem());

        //validar limite diario
        validaLimiteDiario(transacao, utilsRemessa);

        //fazer a cotação e conversão da moeda
        Double valorBr = transacao.getValor();
        transacao.setValor(converteModeda(transacao.getValor(), utilsRemessa));

        // realizar a transação
        var transacaoOk = transacaoRepository.save(transacaoMapper.toTransacaoModel(transacao));

        //atualiza saldo do remetente
        atualizaSaldo(transacao.getValor(), transacao.getContaOrigem(), valorBr);

        //atualiza saldo do destino
        atualizaSaldoDestino(transacao.getValor(), transacao.getContaDestino(), valorBr);

        return transacaoMapper.toTransacaoEntity(transacaoOk);
    }

    private void validarSaldo(Double valor, String documento) {

        //validar se tem saldo
        var saldo = iCarteiraService.verificarSaldo(documento);
        if (saldo < valor)
            throw new RuntimeException("Saldo insuficiente");
    }

    private void validaLimiteDiario(Transacao transacao, UtilsRemessa utilsRemessa) {

        double soma;

        Double remessasDia = transacaoRepository.findAllByDataTransacao(transacao.getDataTransacao());

        if (remessasDia != null)
            soma = remessasDia + transacao.getValor();
        else
            soma = 0.0;

        if (utilsRemessa.isCPF(transacao.getContaOrigem())) {
            if (soma > cotacaoConfig.getValorCotacaoPf())
                throw new RuntimeException("Valor diario de trnsações exedido para pessoa fisica");
        } else if (soma > cotacaoConfig.getValorCotacaoPj()) {
            throw new RuntimeException("Valor diario de transações exedido para pessoa juridica");
        }

    }

    private double converteModeda(Double valor, UtilsRemessa utilsRemessa) {

        double cotacao;
        if (!utilsRemessa.isDiaDeSemana())
            cotacao = 5.0;   //pegar no cache
        else
            cotacao = iCotacaoService.solicitarCotacao();

        return utilsRemessa.convertToUSD(valor, cotacao);

    }

    private void atualizaSaldo(Double valorTr, String documento, Double valor) {


        var carteira = iCarteiraService.recuperaCarteira(documento);

        carteira.setSaldoUsd(carteira.getSaldoUsd() - valorTr);
        carteira.setSaldoBr(carteira.getSaldoBr() - valor);

        var carteiraAtualizada = iCarteiraService.atualizarCarteira(carteira.getIdCarteira(), carteira.getSaldoBr(), carteira.getSaldoUsd());
    }

    private void atualizaSaldoDestino(Double valorTr, String documento, Double valor) {


        var carteira = iCarteiraService.recuperaCarteira(documento);

        carteira.setSaldoUsd(carteira.getSaldoUsd() + valorTr);
        carteira.setSaldoBr(carteira.getSaldoBr() + valor);

        var carteiraAtualizada = iCarteiraService.atualizarCarteira(carteira.getIdCarteira(), carteira.getSaldoBr(), carteira.getSaldoUsd());
    }
}
