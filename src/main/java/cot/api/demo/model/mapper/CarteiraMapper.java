package cot.api.demo.model.mapper;

import cot.api.demo.entity.CarteiraEmntity;
import cot.api.demo.model.Carteira;
import lombok.Generated;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Generated
@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface CarteiraMapper {

    CarteiraEmntity toCarteiraModel(Carteira carteira);
    Carteira toCarteiraEntity(CarteiraEmntity carteiraEmntity);
}
