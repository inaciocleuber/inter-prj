package cot.api.demo.adapter.impl;

import cot.api.demo.adapter.ICotacaoAdapter;
import cot.api.demo.adapter.client.CotacaoClient;
import cot.api.demo.model.CotacaoDiaria;
import cot.api.demo.model.ListaCotacaoDiaria;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CotacaoAdapterImpl implements ICotacaoAdapter {

    @Autowired
    private CotacaoClient cotacaoClient;
    @Override
    public Double solicitarCotacao() {

        Double cotacao = 0.0;
        ListaCotacaoDiaria cot = cotacaoClient.getCotacaoDolar();

        for(CotacaoDiaria n: cot.getValue())
            cotacao = n.cotacaoCompra;

        return cotacao;
    }
}
