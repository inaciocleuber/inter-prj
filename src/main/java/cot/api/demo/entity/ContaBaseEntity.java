package cot.api.demo.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public abstract class ContaBaseEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    @Column(name = "Nome")
    private String nome;

    @Column(name = "Email", unique = true)
    private String email;

    @Column(name = "Senha", unique = true)
    private String senha;

    @OneToOne
    @JoinColumn (name = "idCarteira")
    private CarteiraEmntity carteira;
}
