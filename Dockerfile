FROM maven:3.9.6-amazoncorretto-17 AS build
WORKDIR /project
ADD pom.xml /project
RUN mvn verify --fail-never
COPY . .
RUN mvn clean compile package -DskipTests

# Add the jar to the final docker image
FROM amazoncorretto:17-alpine AS app
COPY --from=build /project/target/demo-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]