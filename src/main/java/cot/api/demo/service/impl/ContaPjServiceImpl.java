package cot.api.demo.service.impl;

import cot.api.demo.model.ContaPj;
import cot.api.demo.model.mapper.CarteiraMapper;
import cot.api.demo.model.mapper.ContaPjMapper;
import cot.api.demo.repository.CarteiraRepository;
import cot.api.demo.repository.ContaPjRepository;
import cot.api.demo.service.IContaPjService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ContaPjServiceImpl implements IContaPjService {

    @Autowired
    private ContaPjRepository repository;

    @Autowired
    private CarteiraRepository carteiraRepository;

    @Autowired
    private ContaPjMapper mapper;

    @Autowired
    private CarteiraMapper carteiraMapper;

    @Override
    public ContaPj recuperarConta(String cnpj) {


        return mapper.toContaPjModel(repository.findFirstByCnpj(cnpj)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND , "Conta pessoa juridica não encontrada")));
    }

    @Override
    public Double verificaSaldoPj(String cnpj) {

        var conta = repository.findFirstByCnpj(cnpj)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND , "Conta pessoa juridica não encontrada"));


        return conta.getCarteira().getSaldoBr();
    }

    @Transactional
    @Override
    public ContaPj salvarRemessaPj(ContaPj conta) {

        var carteira = carteiraRepository.save(carteiraMapper.toCarteiraModel(conta.getCarteira()));
        var carteiraEntity = mapper.toContaPjEntity(conta);
        carteiraEntity.setCarteira(carteira);

        return mapper.toContaPjModel(repository.save(carteiraEntity));
    }

}
