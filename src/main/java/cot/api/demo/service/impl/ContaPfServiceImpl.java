package cot.api.demo.service.impl;


import cot.api.demo.model.ContaPf;
import cot.api.demo.model.mapper.CarteiraMapper;
import cot.api.demo.model.mapper.ContaPfMapper;
import cot.api.demo.repository.CarteiraRepository;
import cot.api.demo.repository.ContaPfRepository;
import cot.api.demo.service.IContaPfService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ContaPfServiceImpl implements IContaPfService {

    @Autowired
    private ContaPfRepository  repository;

    @Autowired
    private CarteiraRepository carteiraRepository;

    @Autowired
    private ContaPfMapper mapper;

    @Autowired
    private CarteiraMapper carteiraMapper;

    @Override
    public ContaPf recuperarConta(String cpf) {
        return mapper.toContaPfModel(repository.findFirstByCpf(cpf)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND , "Conta pessoa fisica não encontrada")));

    }

    @Transactional
    @Override
    public ContaPf salvarRemessaPf(ContaPf conta) {

        var carteira = carteiraRepository.save(carteiraMapper.toCarteiraModel(conta.getCarteira()));
        var carteiraEntity = mapper.toContaPfEntity(conta);
        carteiraEntity.setCarteira(carteira);

        return mapper.toContaPfModel(repository.save(carteiraEntity));

    }

    @Override
    public Double verificaSaldoPf(String cpf) {

        var conta = repository.findFirstByCpf(cpf)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND , "Conta pessoa fisica não encontrada"));

        return conta.getCarteira().getSaldoBr();
    }


}
