package cot.api.demo.repository;

import cot.api.demo.entity.CarteiraEmntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarteiraRepository extends JpaRepository<CarteiraEmntity, UUID> {

   Optional<CarteiraEmntity> findFirstByRegistro(String registro);
}
