package cot.api.demo.service;

import cot.api.demo.model.Carteira;

import java.util.UUID;

public interface ICarteiraService {

    Carteira atualizarCarteira(UUID id, Double saldoBr, Double saldoUsd);

    Double verificarSaldo(String documento);

    Carteira recuperaCarteira(String registro);

}
