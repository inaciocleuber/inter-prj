package cot.api.demo.controller;

import cot.api.demo.model.Transacao;
import cot.api.demo.service.ITransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/transacao")
public class TransacaoController {


    @Autowired
    private ITransacaoService iTransacaoService;

    @PostMapping
    public ResponseEntity<Object> atualizaSaldo(@RequestBody Transacao transacao){
        var trs = iTransacaoService.realizaTransacao(transacao);
        return ResponseEntity.status(HttpStatus.OK).body(trs);
    }


}
