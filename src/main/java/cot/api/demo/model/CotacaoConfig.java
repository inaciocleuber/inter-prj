package cot.api.demo.model;


import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class CotacaoConfig {

    @Value(("${valor-pf.valor}"))
    private Double valorCotacaoPf;

    @Value(("${valor-pj.valor}"))
    private Double valorCotacaoPj;

}
