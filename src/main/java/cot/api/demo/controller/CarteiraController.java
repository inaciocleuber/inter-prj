package cot.api.demo.controller;

import cot.api.demo.service.ICarteiraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
@RequestMapping("/carteira")
public class CarteiraController {
    @Autowired
    private ICarteiraService iCarteiraService;

    @PostMapping("/{id}/{saldoBr}/{saldoUsd}")
    public ResponseEntity<Object> atualizaSaldo(@PathVariable("id") UUID id,
                                        @PathVariable("saldoBr") Double saldoBr,
                                        @PathVariable("saldoUsd") Double saldoUsd){
        var carteira =  iCarteiraService.atualizarCarteira(id, saldoBr, saldoUsd);

        return ResponseEntity.status(HttpStatus.OK).body(carteira);
    }

    @GetMapping("/{documento}")
    public ResponseEntity<Object> recuperarConta(@PathVariable("documento")String documento){
        var carteira = iCarteiraService.recuperaCarteira(documento);
        return ResponseEntity.status(HttpStatus.OK).body(carteira);
    }

}
