package cot.api.demo.service;


import cot.api.demo.model.ContaPj;

public interface IContaPjService {

    ContaPj salvarRemessaPj(ContaPj conta);

    ContaPj recuperarConta(String cnpj);

    Double verificaSaldoPj(String cnpj);
}
