package cot.api.demo.controller;

import cot.api.demo.model.ContaPj;
import cot.api.demo.service.IContaPjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contaPj")
public class ContaPjController {
    @Autowired
    private IContaPjService service;

    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody ContaPj contaPj){
        var conta = service.salvarRemessaPj(contaPj);
        return ResponseEntity.status(HttpStatus.OK).body(conta);
    }

    @GetMapping("/{cnpj}")
    public ResponseEntity<Object> recuperarConta(@PathVariable("cnpj")String cnpj){
        var conta = service.recuperarConta(cnpj);
        return ResponseEntity.status(HttpStatus.OK).body(conta);
    }
}
