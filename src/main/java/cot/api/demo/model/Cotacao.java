package cot.api.demo.model;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cotacao {

    public Double cotacaoCompra;
    public Double cotacaoVenda;
    public String dataHoraCotacao;

}
