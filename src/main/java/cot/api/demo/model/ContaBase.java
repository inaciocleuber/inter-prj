package cot.api.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContaBase  {

    private UUID id;

    private String nome;

    private String email;

    private String senha;

    private Carteira carteira;
}
