package cot.api.demo.service;

import cot.api.demo.model.ContaPf;

public interface IContaPfService {

    ContaPf salvarRemessaPf(ContaPf conta);

    ContaPf recuperarConta(String cpf);

    Double verificaSaldoPf(String cpf);
}
