package cot.api.demo.adapter.client;

import cot.api.demo.model.ListaCotacaoDiaria;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;



@FeignClient(name = "cotacao", url = "${feign.url.site}")
public interface CotacaoClient {

    @GetMapping()
    ListaCotacaoDiaria getCotacaoDolar();
}